let ctx = document.getElementById('myChart').getContext("2d");

let data = {
  labels: [1,2,3,4,5,6,7,8,9,10],
  datasets: [{
    data: [1,2,3,2,1,2,3,4,5,4],
    fill: false
  }]
};

let options = {
  legend: {
    display: false
  },
  title: {
    display: true,
    text: 'Example Chart'
  },
  tooltips: {
    enabled: false
  },
  responsive: false
};

let config = {
  type: 'line',
  data: data,
  options: options
}

let chart;

const createChart = () => {
  chart = new Chart(ctx, config);
  chart.resize();
}

const change = (newType) => {
  if (typeof newType !== 'string' && newType !== 'line' && newType !== 'bar' && newType !== 'pie') {
    return;
  }

  if (chart) {
    chart.destroy();
  }

  config.type = newType;
  createChart();
}

window.onresize = (event) => {
  chart.resize();
};

createChart();
